import { Component, Input } from '@angular/core';
import { News } from '../news';

@Component({
  selector: 'news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss'],
})
export class NewsListComponent {
  @Input() newsList: News;
  @Input() page: number;
  @Input() totalRecords: number;
}
