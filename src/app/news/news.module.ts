import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import { HeaderModule } from '../header/header.module';
import { NewsListComponent } from './news-list/news-list.component';
import { NewsCardComponent } from './news-card/news-card.component';
import { CommonModule } from '@angular/common';
import { NewsService } from '../../services/news/news.service';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxPaginationModule } from "ngx-pagination";
import { from } from 'rxjs';

@NgModule({
  declarations: [NewsComponent, NewsListComponent, NewsCardComponent],
  imports: [CommonModule, NewsRoutingModule, HeaderModule, NgxSpinnerModule, NgxPaginationModule, BrowserAnimationsModule],
  exports: [NewsComponent],
  providers: [NewsService],
  bootstrap: [NewsComponent],
})
export class NewsModule {}
