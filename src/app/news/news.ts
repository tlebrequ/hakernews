export class News {
  id: number;
  title: string;
  by: string;
  score: number;
  time: number;
constructor(
    id: number,
    title: string,
    by: string,
    score: number,
    time: number
  ) {
    this.id = id;
    this.title = title;
    this.by = by;
    this.score = score;
    this.time = time;
  }
}
