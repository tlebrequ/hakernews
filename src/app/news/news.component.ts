import { Component, OnInit } from '@angular/core';
import { News } from './news';
import { NewsService } from '../../services/news/news.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {

  totalRecords: number;
  page = 1;
  news: News[] = [];
  datasLoaded: Promise<boolean>;

  constructor(private newsService: NewsService, private spinner: NgxSpinnerService) {}

  ngOnInit() {
    this.spinner.show();
    this.getNews();
  }

  getNews(): void {
    this.newsService.getNewsId().subscribe((newsId) => {
      newsId.forEach((id, index) =>
        this.newsService.getNews(id).subscribe((item) => {
          const news = new News(
            item.id,
            item.title,
            item.by,
            item.score,
            item.time
          );
          this.news.push(news);
          if (index === newsId.length -1) {
            this.datasLoaded = Promise.resolve(true);
            this.spinner.hide();
          }
        })
      );
      this.totalRecords = newsId.length;
    });
  }
}
