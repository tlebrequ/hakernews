import { NgModule } from '@angular/core'

import { HeaderComponent } from './header.component'
import { NavModule } from '../nav/nav.module'
import { CommonModule } from '@angular/common'

@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    NavModule
  ],
  exports: [
    HeaderComponent
  ],
  providers: [],
  bootstrap: [HeaderComponent]
})
export class HeaderModule { }
