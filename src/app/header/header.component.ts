import { Component } from '@angular/core';
import { navLink } from '../nav/nav.component';

@Component({
  selector: 'header-app',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
  links: navLink[] = [
    new navLink('new', '/new'),
    new navLink('past', '/past'),
    new navLink('comments', '/comments'),
    new navLink('ask', '/ask'),
    new navLink('show', '/show'),
    new navLink('jobs', '/jobs'),
    new navLink('submit', '/submit'),
    new navLink('login', '/login')
  ]
}
