import { Component, Input } from '@angular/core';

@Component({
  selector: 'nav-app',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})

export class NavComponent {
  @Input() navLinks: navLink[]
}

export interface navLinkInterface {
  name: String,
  link: String
}

export class navLink implements navLinkInterface {
  name: String;
  link: String;

  constructor( name: String, link: String ){
    this.name= name
    this.link= link
  }
}
