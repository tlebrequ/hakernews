import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsComponent } from './news/news.component';
import { PastComponent } from './past/past.component';
import { CommentsComponent } from './comments/comments.component';
import { AskComponent } from './ask/ask.component';
import { ShowComponent } from './show/show.component';
import { JobsComponent } from './jobs/jobs.component';
import { SubmitComponent } from './submit/submit.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: 'news', component: NewsComponent },
  { path: 'past', component: PastComponent },
  { path: 'comments', component: CommentsComponent },
  { path: 'ask', component: AskComponent },
  { path: 'show', component: ShowComponent },
  { path: 'jobs', component: JobsComponent },
  { path: 'submit', component: SubmitComponent },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
