import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewsModule } from './news/news.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxPaginationModule } from "ngx-pagination";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PastComponent } from './past/past.component';
import { CommentsComponent } from './comments/comments.component';
import { AskComponent } from './ask/ask.component';
import { ShowComponent } from './show/show.component';
import { JobsComponent } from './jobs/jobs.component';
import { SubmitComponent } from './submit/submit.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    PastComponent,
    CommentsComponent,
    AskComponent,
    ShowComponent,
    JobsComponent,
    SubmitComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NewsModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgbModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
