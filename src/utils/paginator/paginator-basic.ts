import {Component} from '@angular/core';

@Component({
  selector: 'ngbd-pagination-basic',
  templateUrl: './pagination-basic.html'
})
// tslint:disable-next-line: component-class-suffix
export class NgbdPaginationBasic {
  page = 1;
}
