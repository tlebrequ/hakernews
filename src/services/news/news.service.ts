import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { News } from '../../app/news/news';

@Injectable()
export class NewsService {
  apiUrl = 'https://hacker-news.firebaseio.com/v0/';

  constructor(private http: HttpClient) {}

  getNewsId(): Observable<Number[]> {
    return this.http.get<Number[]>(this.apiUrl + 'topstories.json');
  }
  
  getNews(id: Number): Observable<News> {
    return this.http.get<News>(this.apiUrl + 'item/' + id + '.json');
  }
  
}
